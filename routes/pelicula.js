const express = require('express');
const router = express.Router();
const peliculaController = require('../controllers/pelicula');
const loggedIn = require('../middleware/jwtAuth');

/**
 * @swagger
 * /movie:
 *  get:
 *    security: 
 *      - bearerAuth: []
 *    tags: [movies]
 *    description: show all movies
 *    parameters:
 *    - name: authorization
 *      in: header
 *      description: token user
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
 router.get("/", loggedIn, (req, res) => {
    peliculaController.listPelicula()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result
      });
    })
    .catch(error => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400
      });
    });
  });
  
/**
 * @swagger
 * /movie/find:
 *  get:
 *    security: 
 *      - bearerAuth: []
 *    tags: [movies]
 *    description: find movie by titulo
 *    parameters:
 *    - name: authorization
 *      in: header
 *      description: token user
 *      required: true
 *      type: string
 *    - name: titulo
 *      description: titulo
 *      in: query
 *      required: false
 *      type: string
 *    - name: genero
 *      description: genero
 *      in: query
 *      required: false
 *      type: string
 *    responses:
 *       200: 
 *         description: Success
 */
 router.get("/find", loggedIn, (req, res) => {
    peliculaController.findPeliculaInQuery(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result
      });
    })
    .catch(error => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400
      });
    });
  });

/**
 * @swagger
 *   /movie:
 *    post:
 *      security: 
 *      - bearerAuth: []
 *      tags: [movies]
 *      description: create movie
 *      parameters:
 *       - name: authorization
 *         in: header
 *         description: token user
 *         required: true
 *         type: string
 *       - name: imagen
 *         description: imagen
 *         in: formData
 *         required: true
 *         type: string
 *       - name: titulo
 *         description: titulo
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Fecha
 *         description: fecha
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: calificacion
 *         description: calificacion
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: personajes_asociados
 *         description: personajes asociados
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 */
 router.post("/", loggedIn, (req, res) => {
    peliculaController.createPelicula(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch(error => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400
      });
    });
  });
/**
 * @swagger
 *   /movie/{id}:
 *    put:
 *      security: 
 *      - bearerAuth: []
 *      tags: [movies]
 *      description: update pelicula
 *      parameters:
 *       - name: authorization
 *         in: header
 *         description: token user
 *         required: true
 *         type: string
 *       - name: id 
 *         in: path
 *         description: id movie
 *         required: true
 *         type: integer
 *       - name: imagen
 *         description: imagen
 *         in: formData
 *         required: true
 *         type: string
 *       - name: titulo
 *         description: titulo
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Fecha
 *         description: fecha
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: calificacion
 *         description: calificacion
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: personajes_asociados
 *         description: personajes asociados
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
 router.put("/:id", loggedIn, (req, res) => {
    peliculaController.updatePelicula(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch(error => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400
      });
    });
  });
/**
 * @swagger
 *   /movie/{id}:
 *    delete:
 *      security: 
 *      - bearerAuth: []
 *      tags: [movies]
 *      description: delete movie
 *      parameters:
 *       - name: authorization
 *         in: header
 *         description: token user
 *         required: true
 *         type: string
 *       - name: id 
 *         in: path
 *         description: id movie
 *         required: true
 *         type: integer
 *      responses:
 *        200:
 *         description: Success
 */
 router.delete("/:id", loggedIn, (req, res) => {
  peliculaController.deletePelicula(req)
  .then(() => {
    res.status(200).send({
      status: 200,
      message: "Data Delete Successfully",
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to Delete data",
      errors: error,
      status: 400
    });
  });
});

module.exports = router;