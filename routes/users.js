require('dotenv').config();
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users');
/**
 * @swagger
 *   /auth:
 *  get:
 *    security: 
 *      - bearerAuth: []
 *    tags: [users]
 *    description: lista todos los usuarios
 *    parameters:
 *    - name: authorization
 *      in: header
 *      description: token user
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
 router.get("/", (req, res) => {
  usersController.listUser()
  .then((result) => {
    res.status(200).send({
      status: 200,
      message: "Data find Successfully",
      data: result
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to find data",
      errors: error,
      status: 400
    });
  });
});
/**
 * @swagger
 *   /auth/register:
 *    post:
 *      tags: [Users register]
 *      description: create users
 *      parameters:
 *       - name: username
 *         description: name of user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: AccessPassword
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
 router.post("/register", (req, res) => {
  usersController.createUser(req)
  .then(() => {
    res.status(200).send({
      status: 200,
      message: "Data Save Successfully",
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to insert data",
      errors: error,
      status: 400
    });
  });
});
/**
* @swagger
*   /auth/{id}:
*    put:
*      security: 
*      - bearerAuth: []
*      tags: [users]
*      description: actualized users
*      parameters:
*       - name: authorization
*         in: header
*         description: token user
*         required: true
*         type: string
*       - name: id 
*         in: path
*         description: id user
*         required: true
*         type: integer
*       - name: username
*         description: name of user
*         in: formData
*         required: true
*         type: string
*       - name: password
*         description: AccessPassword
*         in: formData
*         required: true
*         type: string
*      responses:
*        200:
*          description: Success
* 
*/
 router.put("/:id", (req, res) => {
  usersController.updateUser(req)
  .then(() => {
    res.status(200).send({
      status: 200,
      message: "Data Save Successfully",
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to insert data",
      errors: error,
      status: 400
    });
  });
});
/**
 * @swagger
 *   /auth/{id}:
*    delete:
*      security: 
*      - bearerAuth: []
*      tags: [users]
*      description: delete users
*      parameters:
*       - name: authorization
*         in: header
*         description: token user
*         required: true
*         type: string
*       - name: id 
*         in: path
*         description: id user
*         required: true
*         type: integer
 *      responses:
 *        200:
 *         description: Success
 */
 router.delete("/:id", (req, res) => {
  usersController.deleteUser(req)
  .then(() => {
    res.status(200).send({
      status: 200,
      message: "Data Delete Successfully",
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to Delete data",
      errors: error,
      status: 400
    });
  });
});

/**
 * @swagger
 *   /auth/login:
 *    post:
 *      tags: [users login]
 *      description: login users
 *      parameters:
 *      - name: username
 *        description: name of user or email
 *        in: formData
 *        requider: true
 *        type: string
 *      - name: password
 *        description: password
 *        in: formData
 *        requider: true
 *        type: string
 *      responses:
 *        200:
 *         description: Success
 */
 router.post("/Login", (req, res) => {
  usersController.Login(req)
  .then(() => {
    const token = jwt.sign({ username: req.body.username }, process.env.JWT_SECRET);
    res.json({token: token});
  })
  .catch(error => {
    res.status(400).send({
      message: "email or password is incorrect",
      errors: error,
      status: 400
    });
  });
});

module.exports = router;