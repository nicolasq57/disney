const express = require('express');
const router = express.Router();
const personajeController = require('../controllers/personaje');
const loggedIn = require('../middleware/jwtAuth');

/**
 * @swagger
 * /character/find:
 *  get:
 *    security: 
 *      - bearerAuth: []
 *    tags: [Character]
 *    description: find character by name
 *    parameters:
 *    - name: authorization
 *      in: header
 *      description: token user
 *      required: true
 *      type: string
 *    - name: nombre
 *      description: nombre
 *      in: query
 *      required: false
 *      type: string
 *    - name: edad
 *      description: edad
 *      in: query
 *      required: false
 *      type: integer
 *    responses:
 *       200: 
 *         description: Success
 */
router.get("/find", loggedIn, (req, res) => {
  personajeController.listPersonajebyname(req)
  .then((result) => {
    res.status(200).send({
      status: 200,
      message: "Data find Successfully",
      data: result
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to find data",
      errors: error,
      status: 400
    });
  });
});


/**
 * @swagger
 * /character:
 *  get:
 *    security: 
 *      - bearerAuth: []
 *    tags: [Character]
 *    description: show all characters
 *    parameters:
 *    - name: authorization
 *      in: header
 *      description: token user
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
 router.get("/", loggedIn, (req, res) => {
  personajeController.listPersonaje()
  .then((result) => {
    res.status(200).send({
      status: 200,
      message: "Data find Successfully",
      data: result
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to find data",
      errors: error,
      status: 400
    });
  });
});

  
/**
 * @swagger
 *   /character:
 *    post:
 *      security: 
 *      - bearerAuth: []
 *      tags: [Character]
 *      description: create character
 *      parameters:
 *       - name: authorization
 *         in: header
 *         description: token user
 *         required: true
 *         type: string
 *       - name: nombre
 *         description: nombre del personaje
 *         in: formData
 *         required: true
 *         type: string
 *       - name: imagen
 *         description: imagen del personaje
 *         in: formData
 *         required: true
 *         type: string
 *       - name: edad 
 *         description: edad del personaje
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: peso
 *         description: peso del personaje
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: historia
 *         description: historia del personaje
 *         in: formData
 *         required: true
 *         type: string
 *       - name: peliculas  
 *         description: peliculas del personaje
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *        200:
 *          description: Success
 * 
 */
 router.post("/", loggedIn, (req, res) => {
  personajeController.createPersonaje(req)
  .then(() => {
    res.status(200).send({
      status: 200,
      message: "Data Save Successfully",
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to insert data",
      errors: error,
      status: 400
    });
  });
});

/**
* @swagger
*   /character/{id}:
*    put:
*      security: 
*      - bearerAuth: []
*      tags: [Character]
*      description: actualized character
*      parameters:
*       - name: authorization
*         in: header
*         description: token user
*         required: true
*         type: string
*       - name: id 
*         in: path
*         description: id character
*         required: true
*         type: integer
*       - name: nombre
*         description: nombre del personaje
*         in: formData
*         required: true
*         type: string
*       - name: imagen
*         description: imagen del personaje
*         in: formData
*         required: true
*         type: string
*       - name: edad 
*         description: edad del personaje
*         in: formData
*         required: true
*         type: integer
*       - name: peso
*         description: peso del personaje
*         in: formData
*         required: true
*         type: integer
*       - name: historia
*         description: historia del personaje
*         in: formData
*         required: true
*         type: string
*       - name: peliculas  
*         description: peliculas del personaje
*         in: formData
*         required: true
*         type: string
*      responses:
*        200:
*          description: Success
* 
*/
router.put("/:id", loggedIn, (req, res) => {
  personajeController.updatePersonaje(req)
  .then(() => {
    res.status(200).send({
      status: 200,
      message: "Data Save Successfully",
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to insert data",
      errors: error,
      status: 400
    });
  });
});

 /**
* @swagger
*   /character/{id}:
*    delete:
*      security: 
*      - bearerAuth: []
*      tags: [Character]
*      description: delete character
*      parameters:
*       - name: authorization
*         in: header
*         description: token user
*         required: true
*         type: string
*       - name: id 
*         in: path
*         description: id character
*         required: true
*         type: integer
*      responses:
*        200:
*         description: Success
*/
router.delete("/:id", loggedIn, (req, res) => {
  personajeController.deletePersonaje(req)
  .then(() => {
    res.status(200).send({
      status: 200,
      message: "Data Delete Successfully",
    });
  })
  .catch(error => {
    res.status(400).send({
      message: "Unable to Delete data",
      errors: error,
      status: 400
    });
  });
});


module.exports = router;