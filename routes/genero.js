const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const genero = require('../models/genero') (connection, Sequelize);



router.get("/", (req, res) => {
    genero.findAll()
    .then((result) => {
        res.status(200).send({
            status: 200,
            message: "Data find Successfully",
            data: result
        });
    })
    .catch(error => {
        res.status(400).send({
            message: "Unable to find data",
            errors: error,
            status: 400
        });
    });
});

module.exports = router;

