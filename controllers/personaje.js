require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const personaje = require('../models/personaje')(connection, Sequelize);

const listPersonaje = async (req) => {
    const result = await personaje.findAll({attributes:
    ['nombre', 'imagen']});
    return result;
}

const listPersonajebyname = async (req) => {
  const { nombre, edad } = req.query;
  const query = {};
  if (nombre) {
    query.nombre = {
      [Sequelize.Op.like]: `%${nombre}%`
    };
  }
  if (edad) {
    query.edad = {
      [Sequelize.Op.like]: `%${edad}%`
    };
  }
  const result = await personaje.findAll({
    where: query,
    attributes: [
      'nombre', 'imagen', 'edad'
    ]
  });
  return result;
}

const createPersonaje = async (req) => {
    const personajeDetails = await personaje.build({
      nombre: req.body.nombre,
      imagen: req.body.imagen,
      edad: req.body.edad,
      peso: req.body.peso,
      historia: req.body.historia,
      peliculas: req.body.peliculas
    });
  
    const result = await personajeDetails.save();
  return result;
}






const updatePersonaje = async (req) => {
  const id_perso = parseInt(req.params.id);
  const result = await personaje.update(
    {
      nombre: req.body.nombre,
      imagen: req.body.imagen,
      edad: req.body.edad,
      peso: req.body.peso,
      historia: req.body.historia,
      peliculas: req.body.peliculas
    },
    { where: { id_personaje: id_perso } }
  );
  return result;
}


const deletePersonaje = async (req) => {
    const id_perso = req.params.id;
    const result = await personaje.destroy({
      where: { id_personaje: id_perso }
    });
    return result;
  }
  
module.exports = {
    listPersonaje,
    createPersonaje,
    updatePersonaje,
    deletePersonaje,
    listPersonajebyname
  }
  