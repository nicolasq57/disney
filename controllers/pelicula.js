require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const pelicula = require('../models/pelicula')(connection, Sequelize);

const listPelicula = async () => await pelicula.findAll(
    {attributes:
      [
        'imagen', 'titulo', 'Fecha'
      ]
    });


const findPeliculaInQuery = async (req) => {
  const { titulo, genero } = req.query;
  const query = {};
  if (titulo) {
    query.titulo = {
      [Sequelize.Op.like]: `%${titulo}%`
    };
  }
  if (genero) {
    query.genero = {
      [Sequelize.Op.like]: `%${genero}%`
    };
  }
  const result = await pelicula.findAll({
    where: query,
    attributes: [
      'imagen', 'titulo', 'Fecha'
    ]
  });
  return result;
}


const createPelicula = async (req) => {
    const peliculaDetails = await pelicula.build({
      imagen: req.body.imagen,
      titulo: req.body.titulo,
      Fecha: req.body.Fecha,
      calificacion: req.body.calificacion,
      personajes_asociados: req.body.personajes_asociados,
      genero: req.body.genero 
    });
    const result = await peliculaDetails.save();
  return result;
}

const updatePelicula = async (req) => {
    const id_Pelicula = parseInt(req.params.id);
    const result = await pelicula.update(
      {
      imagen: req.body.imagen,
      titulo: req.body.titulo,
      Fecha: req.body.Fecha,
      calificacion: req.body.calificacion,
      personajes_asociados: req.body.personajes_asociados 
      },
      { where: { id_pelicula: id_Pelicula} }
    );
    return result;
  }

  const deletePelicula = async (req) => {
    const id_Peliculas = req.params.id;
    const result = await pelicula.destroy({
      where: { id_pelicula: id_Peliculas}
    });
    return result;
  }

module.exports = {
    listPelicula,
    createPelicula,
    updatePelicula,
    deletePelicula,
    findPeliculaInQuery
  }