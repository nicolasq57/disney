require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../config/db.config");
const users = require('../models/users')(connection, Sequelize);

const Login = async (req) => {
    const { username, password } = req.body;
    const user = await users.findOne({
        where: {
            username: username,
            password: password
        }
    });
    return !user ? promise.reject("Invalid email or password") : user; 
}

const createUser = async (req) => {
    const userDetails = await users.build({
      username: req.body.username,
      password: req.body.password,
    });
    const userExists = await users.findOne({
      where: {
        username: req.body.username,
      }
    });
    return userExists ? Promise.reject("User already exists") : userDetails.save();
  };
 
  
const listUser = async () => await users.findAll();

const updateUser = async (req) => {
  const id_users = parseInt(req.params.id);
  const result = await users.update(
    {
      username: req.body.username,
      password: req.body.password,
    },
    { where: { id_user: id_users } }
  );
  return result;
}


const deleteUser = async (req) => {
    const id_users = parseInt(req.params.id);req.params.id;
    const result = await users.destroy({
      where: { id_user: id_users}
    });
    return result;
  }
  
module.exports = {
    createUser,
    listUser,
    updateUser,
    deleteUser,
    Login
}