const assert = require('assert');

const fetch = require('node-fetch');
const url = 'http://localhost:3028/auth/register';


describe('test createUser', async () => {
  it('responde 200', async () => {
      await fetch(url,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify({
                username: 'test',
                password: 'test'
            })
        }).then(res => {
            assert.equal(res.status, 200);
        }
    );
    });
});