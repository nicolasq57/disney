const genero = require('../models/genero');
const pelicula = require('../models/pelicula');
const personaje = require('../models/personaje');
const users = require('../models/users');


pelicula.hasOne(genero, { as: 'genero', foreignKey: 'id_genero' });
genero.hasMany(pelicula, { as: 'peliculas', foreignKey: 'id_genero' });


