const pelicula = (connection, Sequelize) => {
    const Pelicula = connection.define('pelicula', {
        id_pelicula: {
          type: Sequelize.INTEGER,
          primaryKey: true
        },
        imagen: {
          type: Sequelize.STRING
        },
        titulo: {
          type: Sequelize.STRING
        },
        Fecha: {
          type: Sequelize.INTEGER
        },
        calificacion: {
          type: Sequelize.INTEGER
        },
        personajes_asociados: {
          type: Sequelize.STRING
        },
        genero: {
          type: Sequelize.STRING,
        }
    },
    {
      timestamps: false
    });
    return Pelicula
  }
  
  module.exports = pelicula;
  