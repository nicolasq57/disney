const personaje = (connection, Sequelize) => {
    const Personaje = connection.define('personaje', {
        id_personaje: {
          type: Sequelize.INTEGER,
          primaryKey: true
        },
        nombre: {
          type: Sequelize.STRING
        },  
        imagen: {
          type: Sequelize.STRING
        },
        edad: {
          type: Sequelize.INTEGER
        },
        peso: {
          type: Sequelize.INTEGER
        },
        historia: {
          type: Sequelize.STRING
        },
        peliculas: {
          type: Sequelize.STRING
        },
    },
    {
      timestamps: false
    });
    return Personaje
  }
  
  module.exports = personaje;
  