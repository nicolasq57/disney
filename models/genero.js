const genero = (connection, Sequelize) => {
    const Genero = connection.define('genero', {
        id_genero: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nombre: {
            type: Sequelize.STRING
        },
    },
    {
      timestamps: false
    });
    return Genero
  }

module.exports = genero;
