const users = (connection, Sequelize) => {
  const Users = connection.define('users', {
      id_user: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      username: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
  },
  {
    timestamps: false
  });
  return Users
}

module.exports = users;
