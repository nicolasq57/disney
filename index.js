require('dotenv').config()
const express = require('express');
const app = express();
const helmet = require('helmet');
const port = process.env.PORT || 3028;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const users = require('./routes/users');
const personaje = require('./routes/personaje');
const pelicula = require('./routes/pelicula');
const genero = require('./routes/genero');
const sequelize = require('./config/db.config');


const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API disney',
      version: '1.0.0'
    }
  },
  apis: ['./routes/users.js','./routes/personaje.js', './routes/pelicula.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(helmet());

app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

//rutas 
app.use('/auth', users);

app.use('/character', personaje);

app.use('/movie', pelicula);

app.use('/genero', genero);


// sequelize.sync ({force: false})
//   .then(() => {
//     console.log('Base de datos creada');
//   })
//   .catch(error => {
//     console.log(error);
//   });

  

app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});