require('dotenv').config();
const jwt = require('jsonwebtoken');


 
const loggedIn = (req, res, next) => {
    const token = req.headers.authorization.replace('Bearer ','');
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    decoded.username ? next() : res.status(401).send({message: 'Unauthorized'});
}


module.exports = loggedIn;


